const gulp = require('gulp')
const cleanCss = require('gulp-clean-css')
const rename = require('gulp-rename')

gulp.task('css', function() {
	gulp.src(['src/assets/css/*.css'])
		.pipe(cleanCss())
		.pipe(rename({
			suffix: '.min'
		}))
		.pipe(gulp.dest('public/css'))

    return gulp.src(['src/assets/css/vendor/**/*.css'])
        .pipe(gulp.dest('public/css/vendor'))
})
