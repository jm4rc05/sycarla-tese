const gulp = require('gulp')

gulp.task('dir', function() {
    return gulp.src('*.*', {
        read: false
    })
    .pipe(gulp.dest('./log'))
})
