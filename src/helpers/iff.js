const handlebars = require('handlebars');

handlebars.registerHelper('iff', function(a, operator, b, options) {
	var bool = false;
	switch (operator) {
		case '==':
			bool = a == b;
			break;
		case '===':
			bool = a === b;
			break;
		case '!=':
			bool = a != b;
			break;
		case '!==':
			bool = a !== b;
			break;
		case '>':
			bool = a > b;
			break;
		case '>=':
			bool = a >= b;
			break;
		case '<':
			bool = a < b;
			break;
		case '<=':
			bool = a <= b;
			break;
		case '&&':
			bool = a && b;
			break;
		case '||':
			bool = a || b;
			break;
		default:
			throw 'Unknown operator ' + operator;
	}

    var func = new Function();
	if (bool)
		return options.fn(this)
	else
    	if (typeof options.inverse === func)
    		return options.inverse(this)
    	else
    		return null;
});
