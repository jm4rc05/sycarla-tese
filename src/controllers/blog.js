const modPath = require('app-module-path')
modPath.addPath(__dirname + '/lib')
modPath.addPath(__dirname + '/modules')

const fs = require('fs')
const path = require('path')

const express = require('express')
const router = express.Router()

const markdown = require('syn-markdown');
const mt = require('markdown-it-meta');
const md = require('markdown-it')().use(mt);

const sm = require('sitemap')
const robots = require('express-robots-txt')

const config = require('syn-config')

const data = require('syn-boot')()

const sitemap = sm.createSitemap({
    hostname: 'https://www.sci-nuts.com.br',
    cacheTime: 600000,
    urls: [
        { url: '/', changefreq: 'monthly', priority: 0.7 }
    ]
})
const postsList = fs.readdirSync(config.site.postsFolder)
if(postsList) {
    var count = 0
    for(var i = 0; i < postsList.length; i++) {
        var postFile = postsList[i]
        if(postFile.slice(0,1) != config.site.content.servicePrefix) {
            var uri = path.join(config.site.postsFolder, postFile)
            sitemap.add({
                url: uri,
                changefreq: 'monthly',
                priority: 0.3,
                lastmodrealtime: true,
                lastmodfile: uri
            })
        }
    }
}

router.use('/' + config.site.postsFolder, markdown({
    directory: config.site.postsFolder,
    caseSensitive: router.get('case sensitive routing'),
    view: config.site.postsView,
    config: config,
    data: data
}));

router.get('/', function(req, res) {
    res.render('index', {
        config: config,
        data: data
    });
});

router.get('/page/:page', function (req, res){
    res.render(req.params.page, {
        config: config,
        data: data
    });
});

router.get('/list/category/:category', function (req, res){
    var query = "$..[?(@.primaryCategory=='" + req.params.category + "')]";
    res.render('list', {
        paramFilter: query,
        config: config,
        data: data
    });
});

router.get('/list/tag/:tag', function (req, res){
    var query = "$..[?(@.primaryTag=='" + req.params.tag + "')]";
    res.render('list', {
        paramFilter: query,
        config: config,
        data: data
    });
});

router.get('/list/author/:author', function (req, res){
    var query = "$..[?(@.postAuthor=='" + req.params.author + "')]";
    res.render('list', {
        paramFilter: query,
        config: config,
        data: data
    });
});

router.get('/sitemap.xml', function(req, res) {
    sitemap.toXML(function(err, xml) {
        if(err) {
            return res.status(500).end()
        }
        res.header('Content-Type', 'application/xml')
        res.send(xml)
    })
})

router.use(robots({
    UserAgent: '*',
    Disallow: '/posts/_*',
    CrawlDelay: 10,
    Sitemap: 'https://www.sci-nuts.com.br/sitemap.xml'
}))

router.use(function(req, res, next) {
    res.status(404)

    if (req.accepts('hbs')) {
        res.render('404', {
            url: req.url,
            config: config,
            data: data
        })
        return
    }

    if (req.accepts('json')) {
        res.send({
            error: 'Not found'
        })
        return
    }

    res.type('txt').send('Not found')
})

router.use(function(err, req, res, next) {
    if (res.headersSet) {
        return next(err)
    }

    res.status(500)
    res.render('50X', {
        error: err,
        config: config,
        data: data
    })
})

module.exports = router
