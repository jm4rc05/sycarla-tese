const fs = require('fs')
const _ = require('lodash')
require('dotenv').config()

console.info('Reading configuration...')

var defaults = require('../config/default.js')
var env = require('../config/' + ((process.env.NODE_ENV === undefined) ? 'prod' : process.env.NODE_ENV) + '.js')
var config = _.merge({}, defaults, env)

//console.log(require('util').inspect(config, {showHidden: false, depth: null}))

module.exports = config
