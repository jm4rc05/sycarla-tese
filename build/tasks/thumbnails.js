const gulp = require('gulp');
const imageResize = require('gulp-image-resize');
const changed = require('gulp-changed')

gulp.task('thumbnails', function() {
    return gulp.src('src/assets/media/images/posts**/*.+(png|jpg|jpeg|gif)')
        .pipe(changed('public/media'))
        .pipe(imageResize({
            width: 80,
            height: 80,
            crop: true,
            upscale: true
        }))
        .pipe(gulp.dest('public/thumbnails'))
})
