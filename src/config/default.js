module.exports = {
    site: {
        title: 'Tese de Divulgação Científica',
        description: 'Tese de Divulgação Científica',
        language: 'pt',
        port: 8089,
        postsFolder: 'posts',
        postsView: 'post.hbs',
        newsFolder: 'news',
        videosFolder: 'videos',
        defaultLayout: 'default',
        favicon: 'src/assets/media/images/favicon.png',
        youtube: '',
        instagram: 'https://www.instagram.com/sci.nuts',
        twitter: 'https://twitter.com/@NutsSci',
        facebook: '',
        google: '',
        linkedin: '',
        pinterest: '',
        email: 'atendimento@sci-nuts.com.br',
        theme: {
            author: 'C Cursino',
            editor: {
                name: 'Carla Cursino',
                bio: 'Aluna de pós-graduação do Instituto Tecnológico de Aeronáutica - ITA, Carla Cursino é uma jornalista que atualmente se dedica à divulgação científica com projetos institucionais, educacionais e sazonais em diversas áreas de interesse como astronomia, física, química e outras ciências e tecnologias.'
            },
            copyright: '© Copyright 2018',
            about: 'Science Nuts é um trabalho das alunas de pós-graduação em física do Instituto Tecnológico de Aeronáutica - ITA. Nosso objetivo é divulgar a ciência para crianças, jovens e adultos de qualquer idade. Participamos e realizamos eventos voltados à ciência em diversas frentes como astronomia, física, química entre outras.'
        },
        content: {
            firstPageTag: 'FirstPage',
            featuredTag: 'Promoted',
            servicePrefix: '_'
        }
    }
}
