const gulp = require('gulp')
const forever = require('gulp-forever-monitor')

gulp.task('server', ['dir', 'root'], function() {
    forever('src/app.js', {
        env: process.env,
            args: process.args,
            watch: false,
            watchIgnorePatterns: [
                'log/**',
                'node_modules/**'
            ],
            logFile: 'log/forever.log',
            outFile: 'log/out.log',
            errFile: 'log/err.log'
        })
        .on('watch:restart', function() {
            console.info('Application has restarted!\n')
        })
        .on('exit', function() {
            console.error('Application has ended!\n')
        })
})
