const handlebars = require('handlebars');
const jsonPath = require('jsonpath');

/**
 * @function query
 *
 * **Helper**
 *
 * Encapsule JSONPath evaluator as a handlebarsJS helper
 * @see http://jsonpath.com
 *
 * @param {Object} context **[obrigatory]** context data
 *
 * @param {string} query **[obrigatory]** JSONPath query expression
 *                       @see http://goessner.net/articles/JsonPath/index.html#e2
 *
 * @returns {Object} context data filtered by query expression
 *
 * Example:
 *
 * ```
 * {{#each (query context "$.store.book[*].author")}}
 *   {{this}}
 * {{/each}}
 * ```
 *
 * when `context` is:
 * ```
 * { "store": {
 *     "book": [
 *       { "category": "reference",
 *         "author": "Nigel Rees",
 *         "title": "Sayings of the Century",
 *         "price": 8.95
 *       },
 *       { "category": "fiction",
 *         "author": "Evelyn Waugh",
 *         "title": "Sword of Honour",
 *         "price": 12.99
 *       },
 *       { "category": "fiction",
 *         "author": "Herman Melville",
 *         "title": "Moby Dick",
 *         "isbn": "0-553-21311-3",
 *         "price": 8.99
 *       },
 *       { "category": "fiction",
 *         "author": "J. R. R. Tolkien",
 *         "title": "The Lord of the Rings",
 *         "isbn": "0-395-19395-8",
 *         "price": 22.99
 *       }
 *     ],
 *     "bicycle": {
 *       "color": "red",
 *       "price": 19.95
 *     }
 *   }
 * }
 * ```
 *
 * the result is:
 * ```
 *  [
 *   "Nigel Rees",
 *   "Evelyn Waugh",
 *   "Herman Melville",
 *   "J. R. R. Tolkien"
 * ]
 * ```
 *
 */
handlebars.registerHelper('query', function(context, options){
    return jsonPath.query(context, options)
});
