const handlebars = require('handlebars');

handlebars.registerHelper('renderStars', function(v) {
    var ret = '';
    for(i = 1; i < 5; i++) {
        if(i < v)
            ret += '<i class="fa fa-star"></i>\n'
    }
    if((v % Math.floor(v))>0)
        ret += '<i class="fa fa-star-half-full"></i>'

    return new handlebars.SafeString(ret);
});
