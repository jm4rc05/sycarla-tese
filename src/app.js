const modPath = require('app-module-path')
modPath.addPath(__dirname + '/lib')
modPath.addPath(__dirname + '/modules')

const express = require('express')
const favicon = require('serve-favicon')

const fs = require('fs')
const path = require('path')
const rfs = require('rotating-file-stream')

const logger = require('morgan')

const handlebars = require('handlebars')
const expressHandlebars = require('express-handlebars')
const handlebarsHelpers = require('handlebars-helpers')()
const templateHelpers = require('template-helpers')()
const registrar = require('handlebars-registrar')

const config = require('syn-config')

const helmet = require('helmet')

require('dotenv').config()

const app = express()
app.use(helmet({
    contentSecurityPolicy: {
        directives: {
            defaultSrc: ["'self'"],
            childSrc: ["'self'", 'https://*.youtube-nocookie.com'],
            styleSrc: ["'self' 'unsafe-inline'", 'https://*.googleapis.com', 'https://*.cloudflare.com'],
            fontSrc: ["'self'", 'https://*.cloudflare.com', 'https://*.gstatic.com'],
            scriptSrc: ["'self' 'unsafe-inline'", 'https://*.googlesyndication.com', 'https://*.googletagmanager.com'],
            reportUri: '/report-violation',
            upgradeInsecureRequests: true,
        },
    },
    referrerPolicy: {policy: 'same-origin'},
    featurePolicy: {},
    browserSniff: false,
}))

app.use(function(req, res, next) {
    var server = req.protocol + '://' + req.hostname
    if((process.env.NODE_ENV === undefined ? 'prod' : process.env.NODE_ENV) != 'prod') {
        server += ':' + config.site.port
    }

    res.locals.server = server
    res.locals.pathName = req.originalUrl

    next()
})

function pad(num) {
    return (num > 9 ? "" : "0") + num
}
function generator(time, index) {
    if(! time)
        return generator(new Date(), 0)

    var month  = time.getFullYear() + "" + pad(time.getMonth() + 1)
    var day    = pad(time.getDate())
    var hour   = pad(time.getHours())
    var minute = pad(time.getMinutes())

    return month + "/" + month +
        day + "-" + hour + minute + "-" + index + "-access.log"
}
var logDirectory = path.join(__dirname, '../log')
fs.existsSync(logDirectory) || fs.mkdirSync(logDirectory)
var accessLogStream = rfs(generator, {
    interval: '1d',
    path: logDirectory
})
app.use(logger('combined', {
    stream: accessLogStream
}))

console.log('Registering helpers...')
registrar(handlebars, {
    helpers: 'src/helpers/**/*.js'
})

console.log('Setting application...')
app.engine('.hbs', expressHandlebars({
    helpers: [
        registrar,
        handlebarsHelpers,
        templateHelpers
    ],
    partials: [
        'src/views/partials/**/*.{hbs,js}',
        'src/views/layouts/**/*.hbs'
    ],
    defaultLayout: config.site.defaultLayout,
    layoutsDir: 'src/views/layouts',
    partialsDir: 'src/views/partials',
    extname: 'hbs'
}))
app.set('view engine', '.hbs')
app.set('views', 'src/views')

app.use('/', express.static('public'))

app.use(require('./controllers'))

app.use(favicon(config.site.favicon))

process.on('SIGINT', function() {
    console.log('\nExiting...')
    process.exit()
})

app.listen(config.site.port)
console.info('Running on port', config.site.port, 'at', process.env.NODE_ENV === undefined ? 'prod' : process.env.NODE_ENV)
