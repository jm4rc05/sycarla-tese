const fs = require('fs')

const mt = require('markdown-it-meta')
const md = require('markdown-it')().use(mt)

const config = require('syn-config')

const boot = (function () {
    var posts = []
    var news = []
    var videos = []
    var featured = []
    var firstPage = []
    var tags = {}
    var categories = {}
    var authors = {}

    console.log('Uploading posts...')
    var postsList = fs.readdirSync(config.site.postsFolder)
    if(postsList) {
        var count = 0
        for(var i = 0; i < postsList.length; i++) {
            var postFile = postsList[i]
            if(postFile.slice(0,1) != config.site.content.servicePrefix) {
                var stat = fs.statSync(config.site.postsFolder + '/' + postFile)
                var data = fs.readFileSync(config.site.postsFolder + '/' + postFile, 'utf8')
                var content = md.render(data)
                var meta = md.meta
                meta.dateModified = stat.mtime
                if(meta.tags) {
                    if(meta.tags.includes(config.site.content.featuredTag)) {
                        var featuredIndex = featured.length
                        featured[featuredIndex] = {
                            fileName: postFile,
                            meta: meta,
                            postTitle: meta.title,
                            postAuthor: meta.author,
                            postDate: meta.date
                        }
                    } else {
                        if(meta.tags.includes(config.site.content.firstPageTag)) {
                            var firstPageIndex = firstPage.length
                            firstPage[firstPageIndex] = {
                                fileName: postFile,
                                meta: meta,
                                postTitle: meta.title,
                                postAuthor: meta.author,
                                postDate: meta.date
                            }
                        } else {
                            posts[count] = {
                                fileName: postFile,
                                meta: meta,
                                primaryTag: meta.tags[0],
                                primaryCategory: meta.categories[0],
                                postTitle: meta.title,
                                postAuthor: meta.author,
                                postDate: meta.date
                            }
                            if(meta.tags)
                                for(var j = 0; j < posts[count].meta.tags.length; j++) {
                                    var tag = posts[count].meta.tags[j]
                                    if(!tags[tag]) {
                                        tags[tag] = {
                                            name: tag,
                                            count: 1,
                                            posts: []
                                        }
                                    } else {
                                        tags[tag].count++
                                    }
                                    tags[tag].posts[tags[tag].count - 1] = {
                                        post: posts[count]
                                    }
                                }
                            if(meta.categories)
                                for(var j = 0; j < posts[count].meta.categories.length; j++) {
                                    var category = posts[count].meta.categories[j]
                                    if(!categories[category]) {
                                        categories[category] = {
                                            name: category,
                                            count: 1,
                                            posts: []
                                        }
                                    } else {
                                        categories[category].count++
                                    }
                                    categories[category].posts[categories[category].count - 1] = {
                                        post: posts[count]
                                    }
                                }
                            if(meta.author) {
                                var author = posts[count].meta.author
                                if(!authors[author]) {
                                    authors[author] = {
                                        name: author,
                                        count: 1,
                                        posts: []
                                    }
                                } else {
                                    authors[author].count++
                                }
                                authors[author].posts[authors[author].count - 1] = {
                                    post: posts[count]
                                }
                            }
                            count++
                        }
                    }
                }
            }
        }
    }

    console.log('Uploading news...')
    var newsList = fs.readdirSync(config.site.newsFolder)
    if(newsList) {
        for(var i = 0; i < newsList.length; i++) {
            var newsFile = newsList[i]
            if(newsFile.slice(0,1) != config.site.content.servicePrefix) {
                var data = fs.readFileSync(config.site.newsFolder + '/' + newsFile, 'utf8')
                var content = md.render(data)
                var meta = md.meta
                if(meta) {
                    var newIndex = news.length
                    news[newIndex] = {
                        title: meta.title,
                        date: meta.date,
                        source: meta.source,
                        url: meta.url,
                        lede: meta.lede,
                        image: meta.image
                    }
                }
            }
        }
    }

    console.log('Uploading videos...')
    var videosList = fs.readdirSync(config.site.videosFolder)
    if(videosList) {
        for(var i = 0; i < videosList.length; i++) {
            var videoFile = videosList[i]
            if(videoFile.slice(0,1) != config.site.content.servicePrefix) {
                var data = fs.readFileSync(config.site.videosFolder + '/' + videoFile, 'utf8')
                var content = md.render(data)
                var meta = md.meta
                if(meta) {
                    var videoIndex = videos.length
                    videos[videoIndex] = {
                        date: meta.date,
                        video: meta.video,
                        lede: meta.lede,
                    }
                }
            }
        }
    }

    return {
        posts: posts,
        news: news,
        videos: videos,
        featured: featured,
        firstPage: firstPage,
        tags: tags,
        categories: categories,
        authors: authors
    }
})

//console.log(require('util').inspect(boot, {showHidden: false, depth: null}))

module.exports = boot;
