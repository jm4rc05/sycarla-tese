const gulp = require('gulp')
const imageMin = require('gulp-imagemin')
const changed = require('gulp-changed')

gulp.task('images', function() {
	return gulp.src('src/assets/media/**/*.+(png|jpg|jpeg|gif|ico)')
		.pipe(changed('public/media'))
		.pipe(imageMin([
			imageMin.gifsicle({
				interlaced: true
			}),
			imageMin.jpegtran({
				progressive: true
			}),
			imageMin.optipng({
				optimizationLevel: 5
			}),
			imageMin.svgo({
				plugins: [{
						removeViewBox: true
					},
					{
						cleanupIDs: false
					}
				]
			})
		]))
		.pipe(gulp.dest('public/media'))
})
