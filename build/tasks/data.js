const gulp = require('gulp')
const minify = require('gulp-minify')

gulp.task('data', function() {
	return gulp.src(['src/assets/data/*.json'])
		.pipe(gulp.dest('public/data/'))
})
