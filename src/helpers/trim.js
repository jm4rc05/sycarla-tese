const handlebars = require('handlebars');

/**
 * @function trim
 *
 * Removes leading and trailing spaces from text
 *
 * @return {string}
 *
 * Example:
 *
 * `{{trim stringWithSpaces}}`
 *
 */
handlebars.registerHelper('trim', function(obj) {
    if (obj) { return obj.trim(); }
});
