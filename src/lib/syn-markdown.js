function expressMarkdown(options) {
    var fs = require('fs'),
    	path = require('path'),
    	url = require('url'),
    	mk = require('markdown-it-katex'),
    	mt = require('markdown-it-meta'),
        md = require('markdown-it')()

    md.use(mk, {
    	"throwOnError": false,
    	"errorColor": " #cc0000"
    })
    md.use(mt)

	if (typeof options === 'undefined')
		throw new Error("Missing options argument")

	var dir = options.directory
	if (!dir)
		throw new Error('Missing "directory" value in options')

	dir = path.resolve(dir)

	return function(req, res, next) {
		var file = decodeURIComponent(url.parse(req.url).pathname)
		file = options.caseSensitive ? file : file.toLowerCase()

		if (file.slice(-3) !== '.md' && file.slice(-9) !== '.markdown')
			return next()

		file = dir + file
		file = path.normalize(file)

		if (file.substr(0, dir.length) !== dir)
			return res.sendStatus(400)

		fs.exists(file, function(exists) {
			if (!exists)
				return res.sendStatus(404)

			fs.readFile(file, 'utf8', function(err, data) {
				if (err)
					return res.sendStatus(500)

				if (options.view) {
					if (data) {
						var rawtext = data
						var content = md.render(data)
						var meta = md.meta
					} else {
						var rawtext = ''
						var content = ''
						var meta = ''
					}
					options.context = {}
					options.context.includerawtext = options.includerawtext ? true : false
					options.context.rawtext = rawtext
					options.context.loadepiceditor = options.loadepiceditor ? true : false
					options.context.content = content
					options.context.meta = meta
					options.context.data = options.data
					options.context.config = options.config
					res.render(options.view, options.context)
				} else {
					res.sendStatus(data)
				}
			})
		})
	}
}

module.exports = expressMarkdown;
