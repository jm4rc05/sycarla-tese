---
title: 'Criação de Artigos para o site'
date: 2018-06-15T11:30:47.000Z
author: 'Sy Nuts'
---

# Arquivo de Posts

Os textos contendo os artigos são arquivos do tipo _Markdown_ (terminação ".md") são divididos em duas partes:
* Front-matter
* Texto

## _Front-matter_

O _Front-matter_ é delimitado por duas seqüências de três traços horizontais partindo da primeira posição da primeira linha até a linha final da seção:

```
---


---
```

Dentro desta área são inseridas palavras chave que definirão as propriedades do artigo para o site.

### Palavras-chave

As seguintes palavras-chave são utilizadas para configurar os artigos, sendo que alguns são obrigatórios e sem eles o site não irá apresentar o conteúdo adequadamente. As palavras-chave obrigatórias serão destacadas com `'*'`.

Primeiro apresentamos um modelo completo de _Front-matter_ como exemplo:

```
---
title: "Genghis Khan's Mongol horde probably had rampant Hepatitis B"
date: '2018-05-09T11:30:47.000Z'
author: 'J Gomes'
lede: "Viral DNA recovered from ancient human remains sheds light on HBV's evolutionary past."
tags:
- History
categories:
- Science
image: 'genghis-khan.jpg'
imageLabel: 'Imagem de Genghis Khan - Autor desconhecido'
thumbnail: 'Ogadai_Khan.jpg'
---
```

* **title**`(*)` - Título do artigo. O nome do arquivo não é utilizado em momento algum pelo sistema para ser apresentando ao público do site e ao invés disso o título colocado nas chamadas e páginas é aqui obtido.
* **date**`(*)` - Data de publicação. Esta data é utilizada para se observar a "_idade_" do artigo, de modo a apresentá-lo em ordem decrescente nas listas por _tags_ ou _categorias_. A data deve ser informada do seguinte modo - YYYY-MM-DD HH:mm:ss, e é importante para classificar os artigos em ordem decrescente pela data de publicação, e onde:
  * _YYYY_ - ano
  * _MM_ - mês
  * _DD_ - dia
  * _HH_ - hora
  * _mm_ - minutos
  * _ss_ - segundos
* **author**`(*)` - O autor do artigo, também utilizado para agregar os artigos por autor na página do mesmo.
* **lede**`(*)` - Seção introdutória que é apresentada nas listas resumo
* **tags**`(*)` - Lista de _tags_ para o artigo. As _tags_ são utilizadas como taxonomia para classificar os artigos por espécie. As _tags_ são também utilizadas para construir o menu de artigos na barra superior do site, logo abaixo do logotipo. Um limite no número de tags deve ser observado de modo a caberem na linha de menus. Um mesmo artigo pode conter mais de uma _tag_, fazendo com que o mesmo seja listado mais de uma vez em cada lista.
* **categories** - São uma segunda estrutura taxonômica utilizada para criar canais especializados e formatar as chamadas na página principal. O site irá apresentar o artigo mais recente de cada categoria em destaque na página principal, seguido dos artigos menos recentes em diante. Artigos sem categoria não serão listados em nenhum dos canais, porém continuam sendo listados por sua _tag_.
* **image** - A imagem de destaque para o artigo. Apesar de não ser obrigatória é bastante recomendável. Se o artigo pertencer à uma categoria, a ausência de imagem irá criar um espaço vazio na página principal.
* **imageLabel** - Uma descrição da imagem que será apresentada na página do artigo.
* **thumbnail**`(*)` - imagem miniaturizada para ser apresentada nas listas do artigo.

#### _Tags_ especiais

Algumas _tags_ são especiais e são utilizadas para criarem destaque na página principal. Estas _tags_ *não criam ítens de menu* e portanto devem ser adicionadas a outras tags para o artigo ser devidamente classificado. Como estes artigos fazem parte da primeira página, também não é necessário colocá-los em nenhuma categoria pois eles já serão destaque.

* **Promoted** - O artigo será destacado no início da página na _roleta de slides_ logo abaixo do menu. Podemos colocar quantos artigos "promovidos" quanto desejarmos, porém um número excessivamente grande irá sobrecarregar a página principal e a roleta.
* **FirstPage** - O artigo será destacado no lado direito da _roleta de slides_, num limite de até dois artigos.

## Texto

O texto vem logo depois da _Front-matter_. Uma linha em branco entre aquela e o texto é necessária para o sistema de processamento de textos poder separar os dois elementos.

O texto é formatado utilizando uma linguagem simples chamada _Markdown_. Para conhecer melhor essa linguagem de formatação basta consultar o link http://commonmark.org/help/ com uma apresentação simples e rápida dos elementos que podem ser utilizados.

## LaTeX

O site utiliza o KaTeX como pré-processador LaTeX. Com ele é possível inserir fórmulas dentro dos parágrafos ou em destaque no texto. Para isto basta delimitar a fórmula com `$` e inserir a fórmula utilizando LaTeX entre os símbolos - por exemplo:

```
$\frac{1}{r}$
```

A lista de símbolos suportados pode ser encontrada no site do [KaTeX](https://khan.github.io/KaTeX/function-support.html).

## Vídeos Youtube

Os vídeos não podem ser adicionados diretamente, porém podemos adicionar uma imagem com um link para o vídeo assim:

```
[![IMAGE ALT TEXT HERE](http://img.youtube.com/vi/nr5Pj6GQL2o/0.jpg)](https://www.youtube-nocookie.com/embed/nr5Pj6GQL2o)
```

Vai ficar assim:

[![IMAGE ALT TEXT HERE](http://img.youtube.com/vi/nr5Pj6GQL2o/0.jpg)](https://www.youtube-nocookie.com/embed/nr5Pj6GQL2o)

Basta substituir o código `nr5Pj6GQL2o` pelo do vídeo desejado.
