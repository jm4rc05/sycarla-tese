const handlebars = require('handlebars');

// Applies logical conjuction to an arbitrary number of parameters,
// and show enclosed block if true
//
// example:
//
// {{#and conditionOne conditionTwo}}
// This will display if conditionOne and conditionTwo are truthy
// {{/and}}
handlebars.registerHelper('and', function() {
    // `arguments` is not a real Array (therefore doesn't have Array's methods).
    // This converts it into an array.
    var args = Array.prototype.slice.call(arguments),
        // The last argument is always the `options` object.
        options = args.pop();

    for(var i = 0; i < args.length; i++) {
        if(!args[i]) { return; }
    }

    return options.fn(this);
});
