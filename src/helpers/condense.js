const handlebars = require('handlebars');

/**
 * @function condense
 *
 * Shortens a string
 *
 * An optional maximum string length can be provided if preferred.
 *
 * @param {number} maxlen **[optional]** Maximum allowed string length. Default: `10`
 *
 * @param {number} fuzz The allowable deviation from the maxlen, used to allow a sentence/word to complete if it is less than fuzz characters longer than the maxlen
 *
 * @param {string} truncation **[optional]** The truncation string. Default: `'...'`
 *
 * Example:
 *
 * `{{condense myString maxlen="135" truncation="..."}}`
 *
 * This will output the value of `myString` up to a maximum of 135 characters
 * (not including the length of the truncation string) and then append
 * the truncation string to the output
 *
 */
handlebars.registerHelper("condense", function(s, params) {
    var maxlen = 0, fuzz = 0;
    var trunc = params.hash.truncation || '...';

    if (params.hash.maxlen) {
        maxlen = parseInt(params.hash.maxlen, 10);
    }

    // the "fuzz" is an allowable deviation from the maxlen,
    // allowing a sentence/word to complete if it is less than
    // fuzz characters over the maxlen
    if (params.hash.fuzz) {
        fuzz = parseInt(params.hash.fuzz, 10);
    }

    if (!s) { return ''; }

    if (fuzz > maxlen) {
        fuzz = 0;
    }

    // try to end on a sentence, then a word, then just slice
    if (maxlen && s.length > maxlen) {
        var temp_s;
        // return s.substring(0, maxlen) + trunc;
        if (s.length > maxlen && s.lastIndexOf('.', maxlen)+1 !== 0) {
            temp_s = s.substr(0, s.lastIndexOf('.', maxlen)) + trunc;
        } else if (s.length > maxlen-fuzz && s.lastIndexOf(' ', maxlen) !== 0) {
            temp_s = s.substr(0, s.lastIndexOf(' ', maxlen)) + trunc;
        }

        // maxlen - fuzz should be the minimum length of the string
        if (!(temp_s.length < (maxlen + fuzz) && temp_s.length > (maxlen - fuzz))) {
            return s.substring(0, maxlen) + trunc;
        }

        return temp_s;
    }

    return s;
});
