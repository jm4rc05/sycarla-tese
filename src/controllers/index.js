const modPath = require('app-module-path')
modPath.addPath(__dirname + '/lib')
modPath.addPath(__dirname + '/modules')

const express = require('express')

const config = require('syn-config')
const router = express.Router()

console.info('Setting routes...')

router.use('/', require('./blog'))

module.exports = router
