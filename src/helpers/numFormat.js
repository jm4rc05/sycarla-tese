const handlebars = require('handlebars');

/**
 * @function numFormat
 *
 * Delimits a number or string with multiple numbers,
 * using commas or given delimiter
 *
 * Note: This supports integers and decimal numbers.
 *
 * Credit: This function was borrowed from
 * http://cwestblog.com/2011/06/23/javascript-add-commas-to-numbers/
 *
 * @param {string} delimiter **[optional]** The delimiter string. Default: `','`
 *
 * Example:
 *
 * ```
 * {{numFormat num}}
 * {{numFormat num delimiter="." }}
 * ```
 */
handlebars.registerHelper('numFormat', function(num, options) {

    if (!num) { return ''; }

    var delimiter = ',',
        num_string = num.toString();

    if (options && options.hash && options.hash.delimiter) {
        delimiter = options.hash.delimiter;
    }

    return num_string.replace(/\b(\d+)((\.\d+)*)\b/g, function(a, b, c) {
        return (b.charAt(0) > 0 && !(c || '.').lastIndexOf('.') ? b.replace(/(\d)(?=(\d{3})+$)/g, '$1,') : b) + c;
    });
});
