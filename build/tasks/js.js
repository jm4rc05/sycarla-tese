const gulp = require('gulp')
const minify = require('gulp-minify')

gulp.task('js', function() {
	gulp.src(['src/assets/js/*.js'])
		.pipe(minify({
			ext: {
				min: '.min.js'
			},
			noSource: true
		}))
		.pipe(gulp.dest('public/js'))

    return gulp.src(['src/assets/js/vendor/**/*.js'])
        .pipe(gulp.dest('public/js/vendor'))
})
