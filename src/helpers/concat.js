const handlebars = require('handlebars');

/**
 * @function concat
 *
 * **Block Helper**
 *
 * Concatenates all the elements in a collection (array of strings)
 *
 * An optional item separator can be appended to
 * each item and an optional conjunction can be
 * used for the last item.
 *
 * Note: Technically, the elements can be of any type, but it is assumed that the block returns a string
 *
 * @param {string} sep  **[optional]** Item separator. Default: `''`
 *
 * @param {string} conj **[optional]** Final separator, preceeds last item. Default: `''`
 *
 * @returns {string}
 *
 * Example:
 *
 * ```
 * {{#concat context sep="," conj="and"}}
 *     {{this}}
 *  {{/concat}}
 * ```
 *
 * when `context` is:
 * - `['a']`           returns:  `a`
 * - `['a', 'b']`      returns:  `a and b`
 * - `['a', 'b', 'c']` returns:  `a, b and c`
 *
 */
handlebars.registerHelper('concat', function(context, options) {
    if (!context) { return ''; }

    var sep = options.hash.sep || '',
        conj = options.hash.conj || '',
        len = context.length,
        out = '';

    // some special cases
    if (len === 1) { return options.fn(context[0]); }
    if (len === 2) { return options.fn(context[0]) + conj + options.fn(context[1]); }
    if (len === 3) { return options.fn(context[0]) + sep + ' ' + options.fn(context[1]) + conj + options.fn(context[2]); }

    for (var i=0; i<len; i++){

        if (i == len - 1) {
            out += sep + conj;
         } else if (i>0) {
             out+= sep + ' '; // i is not zero
         }

        out += options.fn(context[i]);
    }
    return out;
});
