---
title: "Critical numbers of intervals"
date: '2019-06-30 19:48:47'
author: 'Jin-Kai Li, Yong--Gao Chen'
lede: 'School of Mathematical Sciences and Institute of Mathematics, Nanjing Normal University, Nanjing 210023, PR China.'
tags:
- Matemática
categories:
-
thumbnail: '/thumbnails/posts/jin-kai.png'
image: '/media/images/posts/jin-kai.png'
imageLabel: 'Author Video. Watch what authors say about their articles'
---

In 2014, Herzog, Kaplan and Lev introduced the critical numbers $cr(r,n)$ of natural intervals $[r,n]$ and determined the values of $cr(1,n)$ and $cr(2,n)$ for all $n$, and for $r\geq 3$, they determined the values of $cr(r,n)$ for $n\leq (3r^2+3r)/2$ and $n\geq(5r^2+r)/2$. In this note, we determine the remaining values of $cr(r,n)$ for $r\geq 3$.

[![Critical numbers of intervals](https://img.youtube.com/vi/hZSI-Ul3K94/0.jpg)](https://www.youtube-nocookie.com/embed/hZSI-Ul3K94)

https://doi.org/10.1016/j.jnt.2016.02.027.
