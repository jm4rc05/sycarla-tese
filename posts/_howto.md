---
title: 'Montagem deste site'
date: 2018-06-18T11:30:47.000Z
author: 'Sy Nuts'
---

# Desconstrução de um site

A desconstrução de um site começa a partir do modelo original, que pode conter de uma a dezenas de páginas.

Também devem ser levados em consideração os arquivos auxiliares do modelo original tais como folhas de estilo (stylesheet em CSS) e scripts (javascript).

Partindo destas premissas, o segundo passo é identificar os elementos repetitivos de cada página. O desenho de sites, em grande parte, delimitam e dedicam áreas da página para funcionalidades específicas tais como:
* Logotipo e nome da instituição ou empresa;
* Área de identificação e acionamento da página de entrada com usuário e senha;
* Caixa de pesquisa para buscar páginas por palavras dentro do site;
* Menus de navegação;
* Propagandas e anúncios;
* Outras funcionalidades diversas de acordo com os objetivos do site.

Em seguida deve ser observada a área principal da página, geralmente logo abaixo os menus e entre barras laterais (se houverem) onde invariavelmente encontramos o objeto do site, textos e imagens e foco principal do mesmo.

Muitas vezes o site possui mais de um estilo de página, contendo informações com ou sem imagens, listas que apontam para outras páginas com informações mais detalhadas, formulários de entrada de dados como para cadastro ou contato ou ainda solicitação de maiores informações, páginas de perfis de autores ou seja lá mais o que for necessário para o site.

Com base nessa desconstrução funcional, iremos em seguida usar uma ferramenta de _templating_ como o <b>Mustache/Handlebars</b> (http://mustache.github.io e https://handlebarsjs.com), <b>Underscore</b> (https://underscorejs.org), <b>EJS</b> (http://www.embeddedjs.com) e o <b>Jade</b> (http://jade-lang.com).

Em nosso site utilizamos o <b>Handlebars</b> por julgarmos ser uma ferramenta mais compreensível e fácil de utilizar, inteligentemente construído sobreo <b>Mustach</b> com uma série grande de elementos auxiliares que facilitam a montagem de páginas baseadas em conteúdo obtido em bancos de dados ou arquivos de texto. Existem dezenas de outras ferramentas semelhantes e a escolha quase sempre é bastante pessoal e feita pelo desenvolvedor ou equipe envolvida no projeto.

O <b>Handlebars</b> será o encarregado de pegar os pedaços especializados que selecionamos em nossa _desconstrução_ e montar páginas inteiras para serem apresentadas pelo browser de quem navegar pelo nosso site.

Outro elemento que será necessário é de um servidor web e um serviço para executar o <b>Handlebars</b>. Em nosso caso estamos utilizando o <b>Node.js</b> mas é possível gerar páginas html estáticas que podem ser servidas por um servidor

## Montagem de uma página interativa

No site para apresentação de vídeos e experimentos utilizamos alguns elementos diferentes para montar a interatividade.

Primeiro escolhemos um fundo neutro para a página, obtendo um gradiente de https://webgradients.com e aplicando-o sobre na folha de estilo para a tag `html`, tornando assim este o fundo da página inteira.

Em seguida selecionamos alguns ícones. Escolhemos utilizar `svg` que é uma linguagem de formatação (assim como o `html`), também baseada no `xml` e cujo único propósito é apresentar vetores na página. Maiores informações podem ser obtidas em https://developer.mozilla.org/en-US/docs/Web/SVG.

Os ícones propriamente ditos foram encontrados via https://glyphsearch.com, e os que usamos foram obtidos de http://fontawesome.io/ e http://ionicons.com/.

Copiamos os ícones escolhidos e colocamos numa pasta própria para termos a liberdade de modificá-los (como mudar a cor de fundo por exemplo). Como exemplo, nosso ícone com uma lâmpada copiado de <b>FontAwesome</b> ficou assim:

```xml
<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 384 512">
    <path
        fill="orange"
        d="M272 428v28c0 10.449-6.68 19.334-16 22.629V488c0 13.255-10.745 24-24 24h-80c-13.255 0-24-10.745-24-24v-9.371c-9.32-3.295-16-12.18-16-22.629v-28c0-6.627 5.373-12 12-12h136c6.627 0 12 5.373 12 12zm-143.107-44c-9.907 0-18.826-6.078-22.376-15.327C67.697 267.541 16 277.731 16 176 16 78.803 94.805 0 192 0s176 78.803 176 176c0 101.731-51.697 91.541-90.516 192.673-3.55 9.249-12.47 15.327-22.376 15.327H128.893zM112 176c0-44.112 35.888-80 80-80 8.837 0 16-7.164 16-16s-7.163-16-16-16c-61.757 0-112 50.243-112 112 0 8.836 7.164 16 16 16s16-7.164 16-16z"/>
</svg>
```

_A única mudança que fizemos com relação ao ícone original foi adicionar a tag_: `fill=orange`.

Em seguida obtemos a imagem de um notebook grande o suficiente para colocarmos em nossa página e utilizá-lo como quadro para apresentação de conteúdo.

Feito isso, utilizamos uma folha de estilo para criar as regiões da página onde ficariam os ícones e o conteúdo.

Para que a apresentação de ícones e conteúdo seja responsivo, optamos por utilizar medidas proporcionais sempre que possível, assim, por exemplo, ao invés de estabelecermos a largura de um elemento em _pixels_, o fizemos com um percentual, que quase sempre é relativo ao tamanho do próprio elemento ou do elemento onde ele está contido ou ainda da página como um todo. Por exemplo:

```css
html {
    background-image: linear-gradient(to top, #09203f 0%, #537895 100%);
    width: 100%;
    height: 100%;
}
```

No exemplo acima vemos que a largura e a altura do elemento (no caso o `html`) é de 100%. No caso, `html` é a página toda e essa proporção é relativa a onde ela está contida, ou seja, no próprio browser. Assim estamos informando ao browser que a página ocupa toda a área disponível. Isso foi feito para indicar que a imagem de fundo (no caso `background-image`) deve ocupar a página toda que por sua vez ocupa toda a área disponibilizada pelo próprio browser.

Em seguida definimos uma área para a imagem do notebook que servirá como quadro delimitador de conteúdo:

```html
<div class="cinema">
    <img class="center under screen" src="./images/mbp.png">
    <div id="video">
        <img class="over" src="./images/static.png">
    </div>
</div>
```

Dentro da folha de estilos definimos os tamanhos e proporções:

```css
.cinema {
    position: relative;
    left: 0px;
    top: 0px;
}

.screen {
    display: block;
    width: 75%;
    height: auto;
}

.video {
    position: relative;
    padding-bottom: 56.25%; /* 16:9 */
    padding-top: 25%;
    height: 0;
}

.video iframe {
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
}
```

Observe a construção `.video iframe` onde o vídeo é modificado quando dentro de uma tag `iframe` do `html`. O vídeo é inserido dentro da área delimitada pela classe `video` por meio de _javascript_, inserindo uma `iframe` dentro da div identificada por "_video_". Quando o vídeo não está passando, é mantida a imagem de estática nesta mesma área.

```javascript
function startVideo(video) {
    var iframe = '<iframe class="over" width="100%" height="75%" src="' + video + '?rel=0&autoplay=1" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>';
    document.getElementById("video").innerHTML = iframe;
}

function closeVideo() {
    var img = '<img class="over" src="./images/static.png">'
    document.getElementById("video").innerHTML = img;
}
```

Onde cada "_classe_" representa o elemento que contém a área que estamos delimitando ou formatando. Assim `cinema` é a área como um todo, `screen` é a imagem do notebook que representa nossa tela e `video` é a própria tela do notebook. Utilizamos ainda alguns elementos auxiliares (`center`, `under` e `over`) para alinhamento dos elementos um dentro do outro e fazer o efeito de liga/desliga do vídeo:

```css
.under {
    z-index: -1;
}

.over {
    position: absolute;
    display: block;
    margin-left: auto;
    margin-top: auto;
    left: 20%;
    top: 11%;
    width: 60%;
    z-index:-1;
}

.center {
    display: block;
    margin-left: auto;
    margin-right: auto;
    width: 50%;
}
```

Para completar temos a barra de ícones (não uma barra propriamente dita, mas os ícones alinhados). É criada uma série de elementos, um dentro do outro, construindo a barra e as áreas onde os ícones serão inseridos:

```html
<div class="interaction-wall">
    <div class="interaction-brick">
        <a href="#" onclick="startVideo('./videos/211_DE.mp4')"><span class="interaction-box-chat" data-micron="pop"></span></a>
    </div>

. . .

    <div class="interaction-brick">
        <a href="#" onclick="closeVideo()"><span class="interaction-box-off" data-micron="pop"></span></a>
    </div>
</div>
```

O último ícone é o responsável por executar a função `closeVideo()` que coloca a imagem de estática na área delimitada pela `div id="imagem"`. Já a função `startVideo(...)` insere uma `iframe` dentro da mesma `div`, porém contendo o link do vídeo que desejamos apresentar. Esta `iframe` é formatada pela folha de estilo para apresentar o vídeo extamente onde vemos a tela de um notebook na página.
