const handlebars = require('handlebars');

/**
 * @function loop
 *
 * **Block Helper**
 *
 * Counts from zero to the value of `context` (assuming `context` is a **number**)
 * applying the content of the block each time.
 *
 * Note: A maximum of 100 loops is allowed.
 *
 * Example:
 *
 * ```
 * {{#loop star_rating}}
 *     <img src="{{star}}" class="star"></span>
 * {{/loop}}
 * ```
 *
 */
handlebars.registerHelper('loop', function(numLoops, block) {
    var ret, data;

    numLoops = Math.min(numLoops, 100);

    /* provide index to inner block */
    if (block.data) {
        data = handlebars.createFrame(block.data);
    }

    ret = '';

    for (var i=0; i<numLoops; i++) {
        if (data) { data.index = i; data.max = numLoops; }
        ret += block.fn(this, {data: data});
    }
    return ret;
});
